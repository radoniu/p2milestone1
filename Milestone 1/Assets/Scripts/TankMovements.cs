﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovements : MonoBehaviour {

    public float moveSpeed;
    public float turnSpeed;
    private Transform tf;


    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            tf.Rotate(0, 0, turnSpeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            tf.Rotate(0, 0, -turnSpeed);

        }
        if (Input.GetKey(KeyCode.UpArrow))
        {

            tf.Translate((Vector3.up * moveSpeed), Space.Self);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            tf.Translate((Vector3.down * moveSpeed), Space.Self);

        }

    }
}


