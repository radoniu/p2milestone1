﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public Rigidbody2D rb;
    public float enemySpeed;
    public float enemyTurn;


	// Use this for initialization
	void Start () {
        Vector2 speed = new Vector2(Random.Range(-enemySpeed, enemySpeed), Random.Range(-enemySpeed, enemySpeed));
        float turn = Random.Range(-enemyTurn, enemyTurn);

        rb.AddForce(speed);
        rb.AddTorque(turn);

	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
